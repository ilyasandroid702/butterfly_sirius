#include <cppmisc/traces.h>
#include <cppmisc/argparse.h>
#include <cppmisc/threads.h>
#include "../src/servo_iface.h"
#include "../src/filters.h"


int feedback_loop(Json::Value const& jscfg)
{
    set_thread_rt_priotiy(-1, 90);

    auto servo = ServoIfc::capture_instance();
    servo->init(jscfg);
    servo->start();

    int64_t t;
    double theta, dtheta;
    Integrator I_theta;
    bool stop = false;

    const double max_torque=0.03,
    J=0.73e-3,
    m=21.0/1000.0,
    g=9.81,
    l=13.0/100.0,
    c=m*g*l,
    Eref=2*c,
    a=J+m*l*l;

    double k=2.0;
    while (!stop)
    {
        int status = servo->get_state(t, theta, dtheta, true);
        if (status < 0)
        {
            err_msg("received corrupted packed");
            return -1;
        }

        double phi = theta - M_PI_2;
        double dphi = dtheta;
        double E=(a/2.0)*dphi*dphi+c*(1-cos(phi));
        double errorE=E-Eref;
        double torque = -k*dphi*errorE;

        torque = clamp(torque, -0.02, 0.02);
        servo->set_torque(torque);
        print_msg("[", t * 1e-6, ", ", phi, ", ", dphi, ", ", torque, "],");
    }

    servo->set_torque(0.0);
    servo->stop();
    return 0;
}

int main(int argc, char const* argv[])
{
    Arguments args({
        Argument("-c", "config", "path to json config file", "", ArgumentsCount::One)
    });

    int status = 0;

    try
    {
        auto&& m = args.parse(argc, argv);
        Json::Value const& cfg = json_load(m["config"]);
        traces::init(json_get(cfg, "traces"));
        status = feedback_loop(cfg);
    }
    catch (exception const& e)
    {
        err_msg(e.what());
        status = -1;
    }
    catch (...)
    {
        err_msg("Unknown error occured");
        status = -1;
    }

    return status;
}


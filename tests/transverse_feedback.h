#pragma once

#include "../src/parameters.h"


double phi_star_sp(double t, int der=0);
double servo_constraint(double phi, int n=0);
double dphi_sq_sp(double phi, int der=0);
double proj_op_sp(double phi, int der=0);
Vec3 K_sp(double t, int der=0);


class TransverseFeedback
{
private:
    double _phi_min, _phi_max;
    double _period;

public:
    TransverseFeedback() {}
    ~TransverseFeedback() {}

    Vec4 get_transverse(Vec4 const& state)
    {
        double theta = state(0), phi = state(1), 
            dtheta = state(2), dphi = state(3);

        Vec4 tau_xi;
        tau_xi(1) = theta - servo_constraint(phi);
        tau_xi(2) = dtheta - servo_constraint(phi, 1) * dphi;
        tau_xi(3) = sq(dphi) - dphi_sq_sp(phi);

        if (phi < _phi_min)
        {
            tau_xi(0) = 0;
            return tau_xi;
        }
        if (phi > _phi_max)
        {
            tau_xi(0) = _period / 2;
            return tau_xi;
        }

        if (dphi > 0)
        {
            tau_xi(0) = proj_op_sp(phi);
        }
        else
        {
            tau_xi(0) = proj_op_sp(phi);
        }

        return tau_xi;
    }

    double process(Vec4 const& state)
    {
        auto d = mech_dynamics(state);
        double phi = state(1);
        double dphi = state(3);

        // linear system feedback
        auto tau_xi = get_transverse(state);
        double tau = tau_xi(0);
        Vec3 xi = tau_xi.block<3>(1);
        Vec3 K = K_sp(tau);
        double v = K * xi;

        // input transform
        Vec2 B_perp = {0., 1.};
        double Theta = servo_constraint(phi);
        double Theta1 = servo_constraint(phi, 1);
        double Theta2 = servo_constraint(phi, 2);
    
        Mat2 Mi = inverse(d.M);
        Vec2 dq = state.block<2>(2);
        Vec2 n = {1., -Theta1};
        double a = n * (Mi * (-d.C * dq - d.G)) - Theta2 * sq(dphi);
        double b = n * (Mi * d.B);
        double u = 1/b * v - a/b;
        return u;
    }
};

#include <cppmath/math.h>


namespace parameters {
  static const double m_ball = 3.000000e-03;
  static const double J_ball = 1.300000e-06;
  static const double R_ball = 1.700000e-02;
  static const double J_plates = 7.200000e-04;
  static const double plates_dist = 2.700000e-02;
  static const double gravity_accel = 9.810000e+00;
};

struct MechDynamics {
  Mat2x2 M;
  Mat2x2 C;
  Vec2 G;
  Vec2 B;
};


Vec2 rho_sp(double phi, int der=0);
MechDynamics mech_dynamics(Vec4 const& state);

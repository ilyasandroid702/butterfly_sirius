import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline, BSpline
from scipy.integrate import ode
from dataclasses import dataclass, field, asdict
import json
import numbers


def format_cpparray(arr):
    if isinstance(arr, numbers.Number):
        return '%.6f' % arr
    return '{%s}' % ', '.join([format_cpparray(e) for e in arr])


@dataclass
class ButterflyParameters:
  m_ball : float
  J_ball : float
  R_ball : float
  plates_shape : np.array
  J_plates : float
  plates_dist : float
  gravity_accel : float

  def __post_init__(self):
    self.R_ball_eff  = np.sqrt(self.R_ball**2 - self.plates_dist**2 / 4)


def load_parameters(filepath : str):
  with open(filepath, 'r') as f:
    data = json.load(f)
    pr = ButterflyParameters(
        m_ball = float(data['m_ball']),
        J_ball = float(data['J_ball']),
        R_ball = float(data['R_ball']),
        plates_shape = np.array(data['plates_shape'], float),
        J_plates = float(data['J_plates']),
        plates_dist = float(data['plates_dist']),
        gravity_accel = float(data['g'])
    )
    return pr


def antideriv(f, F1, knots):
  rhs = lambda t,_: f(t)
  solver = ode(rhs)
  solver.set_integrator('dopri5', atol=1e-6, rtol=1e-6)
  solver.set_initial_value(F1, knots[0])

  F = np.zeros(np.shape(knots) + np.shape(F1))
  F[0] = F1

  for i in range(1, len(knots)):
    solver.integrate(knots[i])
    assert np.allclose(knots[i], solver.t)
    F[i] = solver.y
  
  return F


def fix_continuity(arr, period):
  n = len(arr)
  for i in range(1, n):
    if arr[i] - arr[i-1] > period / 2:
      arr[i] -= period
    if arr[i] - arr[i-1] < -period / 2:
      arr[i] += period


def construct_rho_curve(pr : ButterflyParameters):
  delta = pr.plates_shape
  ddelta = np.zeros(delta.shape)
  ddelta[1:] = delta[1:,:] - delta[:-1,:]
  dt = np.linalg.norm(ddelta, axis=1)
  t = np.cumsum(dt)
  delta_fun = make_interp_spline(t, delta, k=3)

  R = pr.R_ball_eff
  delta_tan = delta_fun(t, 1)
  delta_tan = delta_tan / np.reshape(np.linalg.norm(delta_tan, axis=1), (-1, 1))
  delta_nor = np.array([-delta_tan[:,1], delta_tan[:,0]]).T
  rho = delta + delta_nor * R

  drho = np.zeros(rho.shape)
  drho[1:] = rho[1:,:] - rho[:-1,:]
  dt = np.linalg.norm(drho, axis=1)
  t = np.cumsum(dt)

  # 1. find s(phi)
  phi = np.arctan2(rho[:,0], rho[:,1])
  fix_continuity(phi, 2*np.pi)
  rho_fun = make_interp_spline(phi, rho, k=3, bc_type='clamped')
  rho_fun.extrapolate = 'periodic'
  l = lambda phi: np.linalg.norm(rho_fun(phi, 1))
  s = antideriv(l, 0., phi)
  s_fun = make_interp_spline(phi, s, k=3)


  # s(phi) = s_fun
  # rho(phi) = rho_fun

  # print(s_fun(0.5))
  # print(rho_fun(0.5))

  # print('static const double t[] =', format_cpparray(s_fun.t))
  # print('static const double c[] =', format_cpparray(s_fun.c))
  # print('static const double c[] =', s_fun.k)

  # 2. find rho(s)
  rho_fun = make_interp_spline(s, rho, k=3)
  # print('static const double t[] =', format_cpparray(rho_fun.t))
  print('static const double c[] =', format_cpparray(rho_fun.c))
  # print('static const double c[] =', rho_fun.k)


if __name__ == '__main__':
  pr = load_parameters('configs/parameters.json')
  construct_rho_curve(pr)


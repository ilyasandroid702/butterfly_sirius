#!/bin/bash

set -e
cd build
make
cd ..
./build/test_energy_control -c configs/config.json
